require('dotenv').config();

const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./users.json');

app.listen(port);
  console.log('Node JS escuchando en el puerto' + port);

app.get('/holamundos',
    function(request, response){
        console.log('Hola Mundo');
        response.send('Hola Peru');
});

app.use(body_parser.json());

app.get(URL_BASE + 'users',
    function(request, response){
        response.status(200);
        response.send(usersFile);
    });

app.get(URL_BASE + 'users/:id',
    function(request, response){
      console.log(request.params.id);
      let pos = resquest.params.id - 1;
      let respuesta =
          (usersFile[pos] == undefined) ? {"msg":"no existe usuario"} : usersFile[pos];

      response.send(respuesta);
    });

app.post(URL_BASE + 'users',
    function(req, res){
      console.log('POST a User');
      let tam = usersFile.length;
      let new_user = {
        "ID":tam + 1,
        "first_name":req.body.first_name,
        "last_name":req.body.last_name,
        "email":req.body.email,
        "password":req.body.password
      }
      console.log(new_user);
      usersFile.push(new_user);
      res.send({"msg":"Usuario creado correctamente"});
    });

app.put(URL_BASE + 'users/:id',
  function(req, res){
    console.log('PUT a Users');
    console.log('id:  '+req.params.id);
    let pos = req.params.id-1;
    let new_user= {
      "ID":pos + 1,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    console.log(new_user);
    usersFile[pos]=new_user;
    res.send({"msg":"Usuario modificado"});
  });

  app.delete(URL_BASE + 'users/:id',
      function(req, res){
          console.log('Delete Users');
          console.log('id:     '+req.params.id);
          let pos = req.paras.id-1;
          usersFile.splice(pos, 1);
          res.send({"msg":"Usario Eliminado"});
      });

  app.get(URL_BASE + 'usersq',
      function(req, res){
          console.log(req.query.id);
          console.log(req.query.country);
          res.send({"msg":"Get on Query"});
      });


  app.post(URL_BASE + 'login',
      function(request, response){
        var user = request.body.email;
        var pass = request.body.password;
        for(us of usersFile){
            if(us.email == user){
              if(us.password == pass){
                us.logged = true;
                writeUserDataToFile(usersFile);
                console.log("Login correcto");
                response.send({"msg" : "Login correcto", "id" : us.ID})
              }else {
                console.log("Login incorrecto");
                response.send({"msg" : "Login incorrecto"})
              }
            }
        }
      });

  function writeUserDataToFile(data){
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./users.json", jsonUserData, "utf8",
        function(err){
          if(err){
            console.log(err);
          }else {
            console.log("Datos esctitos");
          }
        });
  };

  app.post(URL_BASE + 'logout/:id',
      function(request, response){
        var id = request.params.id;
        for(us of usersFile){
            if(us.ID == id){
              if(us.logged == true){
                delete us.logged;
                writeUserDataToFile(usersFile);
                console.log("Logout correcto");
                response.send({"msg" : "Logout correcto", "id" : us.ID})
              }else {
                console.log("Logout incorrecto");
                response.send({"msg" : "Logout incorrecto"})
              }
            }
        }
      });

  app.get(URL_BASE + "total_users",
      function(request, response){
          let num_users = 0;
          for(us of usersFile){
              num_users++;
          }
          console.log("numero de usuarios:  " + num_users);
          response.send({"num_usuarios" : num_users})
      });
