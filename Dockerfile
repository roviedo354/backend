# Imagen docker base inicial
FROM  node:latest

# Crear el directorio de trabajo del contenedor Docker
WORKDIR /docker-apitechu

# Copiar archivos del proyecto
ADD . /docker-apitechu

# Instalar dependencias del proyecto
# RUN  npm install --only=production

# Puerto donde exponemos contenedor
EXPOSE 3001

# Comando para lanzar app
CMD ["npm", "run", "prod"]
